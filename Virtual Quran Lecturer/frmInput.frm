VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   1380
   ClientLeft      =   6900
   ClientTop       =   3240
   ClientWidth     =   2955
   LinkTopic       =   "Form1"
   ScaleHeight     =   1380
   ScaleWidth      =   2955
   Begin VB.ComboBox Combo1 
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      ItemData        =   "frmInput.frx":0000
      Left            =   240
      List            =   "frmInput.frx":0002
      TabIndex        =   0
      Text            =   "Input Device"
      ToolTipText     =   "Your input device which will be used"
      Top             =   600
      Width           =   2295
   End
   Begin VB.Label Label3 
      Caption         =   "Which input device would you use ?"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   2295
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
