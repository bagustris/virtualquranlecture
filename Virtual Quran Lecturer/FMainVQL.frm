VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form FMainVQL 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Virtual Qur'an Lecturer"
   ClientHeight    =   4995
   ClientLeft      =   5070
   ClientTop       =   2880
   ClientWidth     =   9480
   DrawMode        =   1  'Blackness
   DrawStyle       =   2  'Dot
   FillColor       =   &H00FF8080&
   ForeColor       =   &H80000011&
   Icon            =   "FMainVQL.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   MousePointer    =   99  'Custom
   NegotiateMenus  =   0   'False
   ScaleHeight     =   20.813
   ScaleMode       =   4  'Character
   ScaleWidth      =   79
   StartUpPosition =   1  'CenterOwner
   Begin VB.ComboBox Combo1 
      Height          =   315
      Left            =   240
      TabIndex        =   17
      Text            =   "Input Device"
      Top             =   600
      Width           =   2295
   End
   Begin VB.CommandButton CmdExample 
      Caption         =   "&Listen"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   5760
      TabIndex        =   9
      ToolTipText     =   "Play the sample of utterances"
      Top             =   2640
      Width           =   855
   End
   Begin VB.Timer Timer1 
      Left            =   9000
      Top             =   0
   End
   Begin VB.CommandButton CmdNext 
      Appearance      =   0  'Flat
      DownPicture     =   "FMainVQL.frx":1CCA
      Height          =   375
      Left            =   4320
      Picture         =   "FMainVQL.frx":20EA4
      Style           =   1  'Graphical
      TabIndex        =   5
      ToolTipText     =   "Go to next task"
      Top             =   840
      Width           =   495
   End
   Begin VB.CommandButton CmdPrevious 
      Appearance      =   0  'Flat
      Height          =   375
      Left            =   3840
      Picture         =   "FMainVQL.frx":21233
      Style           =   1  'Graphical
      TabIndex        =   4
      ToolTipText     =   "Go to previous task"
      Top             =   840
      Width           =   495
   End
   Begin VB.CommandButton CmdAnalyze 
      BackColor       =   &H80000008&
      Caption         =   "&Analyze"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   7320
      MaskColor       =   &H00E0E0E0&
      TabIndex        =   3
      ToolTipText     =   "Abalyzing and calculating your utterances"
      Top             =   2640
      Width           =   855
   End
   Begin VB.CommandButton CmdAdvance 
      Caption         =   "Advance"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Go to Advance Level Modul"
      Top             =   2280
      Width           =   1935
   End
   Begin VB.CommandButton CmdIntermediate 
      Caption         =   "Intermediate"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      Style           =   1  'Graphical
      TabIndex        =   1
      ToolTipText     =   "Go to Intermediate Level Modul"
      Top             =   1800
      Width           =   1935
   End
   Begin VB.CommandButton CmdFundamental 
      Caption         =   "&Basic"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      Style           =   1  'Graphical
      TabIndex        =   0
      ToolTipText     =   "Go to Fundamental Level Modul"
      Top             =   1320
      Width           =   1935
   End
   Begin VB.Frame Frame1 
      Caption         =   " Level "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1935
      Left            =   240
      TabIndex        =   6
      Top             =   960
      Width           =   2295
   End
   Begin MSComctlLib.ProgressBar PrbAnalyze 
      Height          =   495
      Left            =   2880
      TabIndex        =   12
      ToolTipText     =   "Process indicator"
      Top             =   3720
      Width           =   6255
      _ExtentX        =   11033
      _ExtentY        =   873
      _Version        =   393216
      Appearance      =   1
      Enabled         =   0   'False
   End
   Begin VB.Frame Frame3 
      Caption         =   "Utterances-Tajwid"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   240
      TabIndex        =   13
      ToolTipText     =   "Correctness Score of Your Utterances"
      Top             =   3000
      Width           =   2295
      Begin VB.Label LbArab3 
         Caption         =   "Tajwid 2: None"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   960
         Width           =   1935
      End
      Begin VB.Label LbArab2 
         Caption         =   "Tajwid 1: None"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   720
         Width           =   1935
      End
      Begin VB.Label LbArab 
         Caption         =   "Read: None"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   480
         Width           =   1935
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   " Modul"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2895
      Left            =   2880
      TabIndex        =   7
      Top             =   360
      Width           =   6255
      Begin VB.CommandButton CmdRecord 
         Caption         =   "&Record"
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3720
         MaskColor       =   &H00E0E0E0&
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Record your utterances"
         Top             =   2280
         Width           =   735
      End
      Begin VB.PictureBox PctArab 
         BackColor       =   &H8000000E&
         Enabled         =   0   'False
         Height          =   1215
         Left            =   960
         Picture         =   "FMainVQL.frx":215C1
         ScaleHeight     =   1155
         ScaleWidth      =   4275
         TabIndex        =   8
         Top             =   960
         Width           =   4335
      End
      Begin VB.Label LbStatus 
         Caption         =   "Save Mode..."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   1080
         TabIndex        =   11
         Top             =   2280
         Width           =   1695
      End
   End
   Begin VB.Label Label3 
      Caption         =   "Input Device:"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   18
      Top             =   360
      Width           =   2295
   End
   Begin VB.Label Label4 
      Caption         =   "Process (%)"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5520
      TabIndex        =   16
      Top             =   3480
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "100 %"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   8640
      TabIndex        =   15
      Top             =   4320
      Width           =   495
   End
   Begin VB.Label Label1 
      Caption         =   "0 %"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Index           =   0
      Left            =   2880
      TabIndex        =   14
      Top             =   4320
      Width           =   615
   End
   Begin VB.Menu MFile 
      Caption         =   "&File"
      Begin VB.Menu MExit 
         Caption         =   "&Exit"
      End
   End
   Begin VB.Menu MHelp 
      Caption         =   "&Help"
      Begin VB.Menu MAbout 
         Caption         =   "&About"
      End
      Begin VB.Menu MManual 
         Caption         =   "&Manual"
      End
      Begin VB.Menu MTutorial 
         Caption         =   "&Tutorial"
      End
   End
End
Attribute VB_Name = "FMainVQL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const SND_FILENAME = &H20000
Private Declare Function PlaySound Lib "winmm.dll" Alias "PlaySoundA" (ByVal lpszName As String, ByVal hModule As Long, ByVal dwFlags As Long) As Long

' chunk
Private Type CHUNKHEADER
    Chunkname As String * 4
    Chunklen As Long
End Type

' WAV header
Private Type WAVEHEADER
    Rifftype As String * 4
End Type

' Fmt chunk
Private Type FMTCHUNK
    Format As Integer
    NumChannels As Integer
    SampleRate As Long
    BytesPerSecond As Long
    BytesPerSample As Integer ' ignore
    BitsPerSample As Integer
End Type
Dim DevHandle As Long
Dim WaveFileName As String
Dim StatusLevel As Integer
Dim StatusPicture As Integer
Dim Signal() As Double
Dim SignalMax As Double
Dim LengthSignal As Long
Dim k As Long
Dim m As Long
Dim i As Long
Dim StatusRecord As Integer
Private Sub EnumDevices()
' there is function for detect the devices can be used to record and anlizing voices
' list box will show which devices cen be used
    Dim wicCaps As WaveInCaps
    Dim lngNumDev As Long
    Dim ret As Long
    Combo1.Clear
    'find out how many sound devices there are on the system,
    'and get their names.
    For lngNumDev = 0 To waveInGetNumDevs - 1
        ret = waveInGetDevCaps(lngNumDev, VarPtr(wicCaps), Len(wicCaps))
        'If wicCaps.Formats And WAVE_FORMAT_1M08 Then
        If wicCaps.Formats And WAVE_FORMAT_1M08 Then 'Now is 1S08 -- Check for devices that can do stereo 8-bit 11kHz
           Combo1.AddItem StrConv(wicCaps.ProductName, vbUnicode) & Str(lngNumDev)
        End If
    Next
    'If none are found, then do something appropriate...
    If Combo1.ListCount = 0 Then
        'tell the user about it.
        MsgBox "No audio devices were found on your system.", vbCritical
        'grey out the start button to prevent the user forcing the
        'system to do something it can't
        CmdRecord.Enabled = False
        'Jump out of the subroutine...
        Exit Sub
    End If
    'Set the default in the combo as device 0. This will ordinarily
    'be the only one of course as not many people have 2 sound cards.
    Combo1.ListIndex = 0
    'Update the GUI
    DoEvents
End Sub
Private Sub CmdAnalyze_Click()
' in this function, the voice/speech or utterances is analyzed by program
' by extracting the cepstral feature and modelling by GMM the correctness of utterances will
' be calculated
LbStatus.Caption = "Analyzing..."
CmdFundamental.Enabled = False
CmdIntermediate.Enabled = False
CmdAdvance.Enabled = False
CmdNext.Enabled = False
CmdPrevious.Enabled = False
CmdExample.Enabled = False
CmdRecord.Enabled = False
CmdAnalyze.Enabled = False
'Digital signal processing to feature extraction
'Declaration of variable array
Dim WaveFile As Integer
Dim SampleRate As Long
Dim NumChannels As Integer
Dim BitsPerSample As Integer
Dim NumSamples As Long
Dim msg As String
'WaveFileName = "C:\Intermediate X\val10.wav"
PlaySound WaveFileName, 0, SND_FILENAME
WaveFile = FreeFile
Open WaveFileName For Binary As WaveFile
    If Err.Number <> 0 Then
        msg = "Error opening file " & WaveFileName & vbCrLf
        msg = msg + Str$(Err.Number) & " - " & Err.Description
        MsgBox msg
        Exit Sub
    End If
    If GetWaveInfo(WaveFile, SampleRate, NumChannels, BitsPerSample, NumSamples) = True Then
        Dim c As Integer
        Dim i As Long
        ' must be Doubles for AddParametric
        ReDim Y(NumSamples - 1, NumChannels - 1) As Double

        If BitsPerSample = 8 Then
            ReDim ybyte(NumChannels - 1, NumSamples - 1) As Byte
            Get WaveFile, , ybyte   ' read data in one go
            For i = 0 To NumSamples - 1
                For c = 0 To NumChannels - 1
                    Y(i, c) = ybyte(c, i) - 128 '8 Bits/sample is unsigned
                Next
            Next
        ElseIf BitsPerSample = 16 Then
            ReDim yshort(NumChannels - 1, NumSamples - 1) As Integer
            Get WaveFile, , yshort  ' read data in one go
            For i = 0 To NumSamples - 1
                For c = 0 To NumChannels - 1
                    Y(i, c) = yshort(c, i)
                Next
            Next
        Else
            Close WaveFile
            MsgBox BitsPerSample & " bits/sample not supported"
            Exit Sub
        End If
    Else
        MsgBox "Not a valid Wav file"
    End If
Close WaveFile
'Searching maximum value and normalize the signal overall
ReDim Signal(NumSamples)
LengthSignal = NumSamples - 1
SignalMax = CDbl(Abs(Y(1, 0)))
Signal(0) = CDbl(Abs(Y(1, 0)))
For m = 1 To NumSamples - 1
    Signal(m) = Y(m, 0)
    If Abs((SignalMax)) < Abs((Signal(m))) Then
        SignalMax = Abs((Signal(m)))
        Debug.Print "signal max is about "; SignalMax
    End If
Next

Dim LocTextFile As String
LocTextFile = "C:\Temp.txt"
Open LocTextFile For Output As #1
Print #1, SignalMax
For m = 1 To NumSamples - 1
    Signal(m) = Signal(m) / SignalMax
    'Debug.Print "signal is about "; Signal(m)
    Print #1, Signal(m)
Next
Close #1
m = 0
k = 0
'Processing in Frame Blcoking and select the non-silent frame
Dim NumberSampelFrame As Long
Dim NumberFrame As Long
Dim NumberFrameSelected As Long
Dim selectedSignal() As Double
Dim selected() As Double
Dim energySignalSelected() As Double
'Defined parameters signal processing in Frame blocking
NumberSampelFrame = 320
NumberFrame = Round((1.9 * (LengthSignal / NumberSampelFrame)), 0)
PrbAnalyze.Value = 1
LbStatus.Caption = "Frame Blocking in process"
Call FrameBlocking(Signal, NumberSampelFrame, NumberFrame, NumberFrameSelected, selectedSignal, energySignalSelected)
PrbAnalyze.Value = PrbAnalyze.Value + 1
LbStatus.Caption = "DFT ini process"
'DFT in Process..........
Dim NumberDFT As Integer
NumberDFT = 1024
Dim XXR As Double
Dim XXI As Double
Dim Mag() As Double
ReDim Mag(NumberFrameSelected, NumberDFT) As Double
Dim NoSampling As Integer
Dim N As Integer
For m = 1 To NumberFrameSelected
    For k = 1 To NumberDFT
        XXR = 0
        XXI = 0
        For N = 1 To NumberSampelFrame
            XXR = XXR + selectedSignal(m, N) * Cos(2 * 3.14 * k * N / 1024)
            XXI = XXI - selectedSignal(m, N) * Sin(2 * 3.14 * k * N / 1024)
        Next
        Mag(m, k) = Abs(Sqr(Abs(XXR ^ 2 + XXI ^ 2)))
    Next
    PrbAnalyze.Value = PrbAnalyze.Value + 50 / NumberFrameSelected
Next
Erase selectedSignal
'Call DFT(selectedSignal, NumberSampelFrame, NumberDFT, NumberFrameSelected, Mag)
LbStatus.Caption = "MFCC extracting in process"
'Extracting signal in process
Dim NumberMFCC As Integer
NumberMFCC = 14
Dim coeffMFCC() As Double
Dim dMFCC() As Double
Dim ddMFCC() As Double
Call MFCC(Mag, NumberFrameSelected, NumberDFT, NumberMFCC, coeffMFCC, dMFCC, ddMFCC)
PrbAnalyze.Value = PrbAnalyze.Value + 10

'Template matching
LbStatus.Caption = "Calculation Process"
Dim parameter() As Double
Dim determinan() As Double
Dim loglikelihood As Double
Dim jumlahHarokat As Integer
Dim jumlahHarokatAnalis As Integer
Dim jumlahHarokatSebelum As Integer
Dim start As Long
If StatusLevel = 1 Then
    If StatusPicture = 1 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-1.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        Debug.Print "Loglikelihood is about "; loglikelihood
        If loglikelihood > -70 And loglikelihood < -66.85 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 2 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-2.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -71 And loglikelihood < -62 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 3 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-3.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -56 And loglikelihood < -51 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 4 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-4.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -87 And loglikelihood < -83 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 5 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-5.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -54 And loglikelihood < -50 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 6 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-6.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -55 And loglikelihood < -54 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 7 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-7.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -71 And loglikelihood < -68 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 8 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-8.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -87 And loglikelihood < -82 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 9 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-9.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -87 And loglikelihood < -84 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 10 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-10.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -69 And loglikelihood < -65 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 11 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-11.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -50 And loglikelihood < -47 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 12 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-12.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -45 And loglikelihood < -42 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 13 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-13.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -45 And loglikelihood < -42 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 14 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-14.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -53 And loglikelihood < -50 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 15 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-15.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -60 And loglikelihood < -59 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 16 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-16.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -115 And loglikelihood < -114 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 17 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-17.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -72 And loglikelihood < -70 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 18 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-18.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -44 And loglikelihood < -42 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 19 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-19.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -135 And loglikelihood < -131 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 20 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-20.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -53 And loglikelihood < -49 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 21 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-21.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -112 And loglikelihood < -108 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 22 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-22.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -81 And loglikelihood < -79 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 23 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-23.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -115 And loglikelihood < -109 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 24 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-24.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -95 And loglikelihood < -88 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 25 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-25.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -113 And loglikelihood < -107 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 26 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-26.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -119 And loglikelihood < -107 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 27 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-27.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -79 And loglikelihood < -75 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 28 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-28.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -129 And loglikelihood < -120 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    ElseIf StatusPicture = 29 Then
        Open "C:\Virtual Quran Lecturer\Templates\1-29.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        If loglikelihood > -97 And loglikelihood < -91 Then
            MsgBox "Right Utterances, do next"
        Else
            MsgBox "Not a right utterances, please repeat carefully"
        End If
    End If
ElseIf StatusLevel = 2 Then
    If StatusPicture = 1 Then
     jumlahHarokat = 2
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 0
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\2-1-1.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -110 And loglikelihood < -109 Then
            MsgBox "First Harokat is right"
        Else
            MsgBox "First Harokat is wrong"
        End If
     
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 1
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start - 1
        Open "C:\Virtual Quran Lecturer\Templates\2-1-2.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -59 And loglikelihood < -55 Then
            MsgBox "Second Harokat is right"
        Else
            MsgBox "Second Harokat is wrong"
        End If
     
    ElseIf StatusPicture = 2 Then
        jumlahHarokat = 6
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 0
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\2-2-1.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -67 And loglikelihood < -66 Then
            MsgBox "First Harokat is right"
        Else
            MsgBox "First Harokat is wrong"
        End If
     jumlahHarokatAnalis = 2
     jumlahHarokatSebelum = 1
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\2-2-2.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -83 And loglikelihood < -82 Then
            MsgBox "Your tajwid is right"
        Else
            MsgBox "Yaour tajwid is wrong"
        End If
     
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 3
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\2-2-3.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -59 And loglikelihood < -55 Then
            MsgBox "Second Harokat is right"
        Else
            MsgBox "Second Harokat is wrong"
        End If
     
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 4
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\2-2-4.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -27 And loglikelihood < -25 Then
            MsgBox "Third Harokat is right"
        Else
            MsgBox "Third Harokat is wrong"
        End If
     
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 5
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected) - 1
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start - 1
        Open "C:\Virtual Quran Lecturer\Templates\2-2-5.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -22.5 And loglikelihood < -21 Then
            MsgBox "Last Harokat is right"
        Else
            MsgBox "Last Harokat is wrong"
        End If
     
     
    ElseIf StatusPicture = 3 Then
        jumlahHarokat = 2
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 0
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\2-3-1.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -143.5 And loglikelihood < -140 Then
            MsgBox "Your tajwid is right"
        Else
            MsgBox "Your tajwid is wrong"
        End If
     Debug.Print "start: "; start
     Debug.Print "NumberFrame: "; NumberFrame
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 1
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start - 1
        Open "C:\Virtual Quran Lecturer\Templates\2-3-2.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -29.88 And loglikelihood < -28 Then
            MsgBox "Second Harokat is right"
        Else
            MsgBox "Second Harokat is wrong"
        End If
     Debug.Print "start: "; start
     Debug.Print "NumberFrame: "; NumberFrame
     
     
    ElseIf StatusPicture = 4 Then
     jumlahHarokat = 4
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 0
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\2-4-1.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -64.4 And loglikelihood < -63.5 Then
            MsgBox "Tajwid is right"
        Else
            MsgBox "Tajwid is wrong"
        End If
     
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 1
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\2-4-2.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -91 And loglikelihood < -89 Then
            MsgBox "Second Harokat is right"
        Else
            MsgBox "Second Harokat is wrong"
        End If
     
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 2
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\2-4-3.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -35 And loglikelihood < -34 Then
            MsgBox "Third Harokat is right"
        Else
            MsgBox "Third Harokat is wrong"
        End If
     
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 3
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start - 1
        Open "C:\Virtual Quran Lecturer\Templates\2-4-4.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -18 And loglikelihood < -17.5 Then
            MsgBox "Second Harokat is right"
        Else
            MsgBox "Second Harokat is wrong"
        End If
       
    ElseIf StatusPicture = 5 Then
        jumlahHarokat = 5
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 0
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\2-5-1.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -77.5 And loglikelihood < -76 Then
            MsgBox "First Harokat is right"
        Else
            MsgBox "First Harokat is wrong"
        End If
     
     jumlahHarokatAnalis = 2
     jumlahHarokatSebelum = 1
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\2-5-2.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -71.5 And loglikelihood < -70 Then
            MsgBox "Second Harokat is right"
        Else
            MsgBox "Second Harokat is wrong"
        End If
     
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 3
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\2-5-3.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -69 And loglikelihood < -68 Then
            MsgBox "Third Harokat is right"
        Else
            MsgBox "Third Harokat is wrong"
        End If
     
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 4
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start - 1
        Open "C:\Virtual Quran Lecturer\Templates\2-5-4.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -15.9 And loglikelihood < -15.5 Then
            MsgBox "Last Harokat is right"
        Else
            MsgBox "Last Harokat is wrong"
        End If
     
    
    ElseIf StatusPicture = 6 Then
        jumlahHarokat = 4
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 0
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\2-6-1.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -71 And loglikelihood < -73.5 Then
            MsgBox "First Harokat is right"
        Else
            MsgBox "First Harokat is wrong"
        End If
     
     jumlahHarokatAnalis = 2
     jumlahHarokatSebelum = 1
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\2-6-2.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -62 And loglikelihood < -59.7 Then
            MsgBox "Tajwid is right"
        Else
            MsgBox "Tajwid is wrong"
        End If
     
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 3
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start - 1
        Open "C:\Virtual Quran Lecturer\Templates\2-6-3.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -59 And loglikelihood < -55 Then
            MsgBox "Second Harokat is right"
        Else
            MsgBox "Second Harokat is wrong"
        End If
     
    End If
ElseIf StatusLevel = 3 Then
    If StatusPicture = 1 Then
        jumlahHarokat = 5
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 0
     start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
     NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\3-1-1.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -59 And loglikelihood < -58.5 Then
            MsgBox "First Harokat is right"
        Else
            MsgBox "First Harokat is wrong"
        End If
     jumlahHarokatAnalis = 2
     jumlahHarokatSebelum = 1
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\3-1-2.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -85 And loglikelihood < -82.5 Then
            MsgBox "Mad is right"
        Else
            MsgBox "Mad is wrong"
        End If
     
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 3
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\3-1-3.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -23 And loglikelihood < -22 Then
            MsgBox "Fourth Harokat is right"
        Else
            MsgBox "Fourth Harokat is wrong"
        End If
     
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 4
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start - 1
        Open "C:\Virtual Quran Lecturer\Templates\3-1-4.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -22.3 And loglikelihood < -21 Then
            MsgBox "Last Harokat is right"
        Else
            MsgBox "Last Harokat is wrong"
        End If
        
    ElseIf StatusPicture = 2 Then
        jumlahHarokat = 5
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 0
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\3-2-1.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -53.5 And loglikelihood < -52 Then
            MsgBox "First Harokat is right"
        Else
            MsgBox "First Harokat is wrong"
        End If
     jumlahHarokatAnalis = 2
     jumlahHarokatSebelum = 1
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\3-2-2.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -64.5 And loglikelihood < -63 Then
            MsgBox "Tajwid is right"
        Else
            MsgBox "Tajwid is wrong"
        End If
     
     jumlahHarokatAnalis = 2
     jumlahHarokatSebelum = 3
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start - 1
        Open "C:\Virtual Quran Lecturer\Templates\3-2-3.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -50 And loglikelihood < -49 Then
            MsgBox "Last Harokat is right"
        Else
            MsgBox "Last Harokat is wrong"
        End If
    ElseIf StatusPicture = 3 Then
        jumlahHarokat = 5
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 0
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\3-3-1.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -77.2 And loglikelihood < -75 Then
            MsgBox "First Harokat is right"
        Else
            MsgBox "First Harokat is wrong"
        End If
     
     jumlahHarokatAnalis = 2
     jumlahHarokatSebelum = 1
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\3-3-2.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -59 And loglikelihood < -52 Then
            MsgBox "Tajwid is right"
        Else
            MsgBox "Tajwid is wrong"
        End If
     
     jumlahHarokatAnalis = 2
     jumlahHarokatSebelum = 3
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start - 1
        Open "C:\Virtual Quran Lecturer\Templates\3-3-3.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -29.2 And loglikelihood < -28 Then
            MsgBox "Second Harokat is right"
        Else
            MsgBox "Second Harokat is wrong"
        End If
    ElseIf StatusPicture = 4 Then
        jumlahHarokat = 5
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 0
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\3-4-1.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -96.5 And loglikelihood < -95 Then
            MsgBox "First Harokat is right"
        Else
            MsgBox "First Harokat is wrong"
        End If
     
     jumlahHarokatAnalis = 2
     jumlahHarokatSebelum = 1
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\3-4-2.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -52 And loglikelihood < -55 Then
            MsgBox "Tajwid is right"
        Else
            MsgBox "Tajwid is wrong"
        End If
     
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 3
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start
        Open "C:\Virtual Quran Lecturer\Templates\3-4-3.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -34.4 And loglikelihood < -34 Then
            MsgBox "Second Harokat is right"
        Else
            MsgBox "Second Harokat is wrong"
        End If
     
     jumlahHarokatAnalis = 1
     jumlahHarokatSebelum = 4
        start = (jumlahHarokatSebelum / jumlahHarokat * NumberFrameSelected)
        NumberFrame = ((jumlahHarokatAnalis / jumlahHarokat * NumberFrameSelected)) + start - 1
        Open "C:\Virtual Quran Lecturer\Templates\3-4-4.txt" For Input As #1
        Call GetDataTxt(parameter, determinan)
        Close #1
        Call GMMComplicated(start, NumberFrame, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
        'MsgBox "Your loglikelihood is about " & loglikelihood
        If loglikelihood > -24 And loglikelihood < -22 Then
            MsgBox "Second Harokat is right"
        Else
            MsgBox "Second Harokat is wrong"
        End If
    End If
End If

LbStatus.Caption = "Finish"
If PrbAnalyze.Value >= 50 Then
CmdFundamental.Enabled = True
CmdIntermediate.Enabled = True
CmdAdvance.Enabled = True
CmdNext.Enabled = True
CmdPrevious.Enabled = True
CmdExample.Enabled = True
CmdRecord.Enabled = True
CmdAnalyze.Enabled = True
LbStatus.Caption = "Finish Analyzing"
End If
End Sub
Private Sub CmdExample_Click()
LbStatus.Caption = "Play Example..."
Dim i As Long, RS As String, cb As Long, W$
RS = Space$(128)
If StatusLevel = 1 Then
    If StatusPicture = 1 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-1.wav"
        ElseIf StatusPicture = 2 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-2.wav"
        ElseIf StatusPicture = 3 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-3.wav"
        ElseIf StatusPicture = 4 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-4.wav"
        ElseIf StatusPicture = 5 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-5.wav"
        ElseIf StatusPicture = 6 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-6.wav"
        ElseIf StatusPicture = 7 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-7.wav"
        ElseIf StatusPicture = 8 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-8.wav"
        ElseIf StatusPicture = 9 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-9.wav"
        ElseIf StatusPicture = 10 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-10.wav"
        ElseIf StatusPicture = 11 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-11.wav"
        ElseIf StatusPicture = 12 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-12.wav"
        ElseIf StatusPicture = 13 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-13.wav"
        ElseIf StatusPicture = 14 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-14.wav"
        ElseIf StatusPicture = 15 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-15.wav"
        ElseIf StatusPicture = 16 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-16.wav"
        ElseIf StatusPicture = 17 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-17.wav"
        ElseIf StatusPicture = 18 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-18.wav"
        ElseIf StatusPicture = 19 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-19.wav"
        ElseIf StatusPicture = 20 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-20.wav"
        ElseIf StatusPicture = 21 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-21.wav"
        ElseIf StatusPicture = 22 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-22.wav"
        ElseIf StatusPicture = 23 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-23.wav"
        ElseIf StatusPicture = 24 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-24.wav"
        ElseIf StatusPicture = 25 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-25.wav"
        ElseIf StatusPicture = 26 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-26.wav"
        ElseIf StatusPicture = 27 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-27.wav"
        ElseIf StatusPicture = 28 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-28.wav"
        ElseIf StatusPicture = 29 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\1-29.wav"
    End If
ElseIf StatusLevel = 2 Then
    If StatusPicture = 1 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-1.wav"
        ElseIf StatusPicture = 2 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-2.wav"
        ElseIf StatusPicture = 3 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-3.wav"
        ElseIf StatusPicture = 4 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-4.wav"
        ElseIf StatusPicture = 5 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-5.wav"
        ElseIf StatusPicture = 6 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-6.wav"
        ElseIf StatusPicture = 7 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-7.wav"
        ElseIf StatusPicture = 8 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-8.wav"
        ElseIf StatusPicture = 9 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-9.wav"
        ElseIf StatusPicture = 10 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-10.wav"
        ElseIf StatusPicture = 11 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-11.wav"
        ElseIf StatusPicture = 12 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-12.wav"
        ElseIf StatusPicture = 13 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-13.wav"
        ElseIf StatusPicture = 14 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-14.wav"
        ElseIf StatusPicture = 15 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-15.wav"
        ElseIf StatusPicture = 16 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-16.wav"
        ElseIf StatusPicture = 17 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-17.wav"
        ElseIf StatusPicture = 18 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-18.wav"
        ElseIf StatusPicture = 19 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-19.wav"
        ElseIf StatusPicture = 20 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\2-20.wav"
    End If
ElseIf StatusLevel = 3 Then
    If StatusPicture = 1 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\3-1.wav"
        ElseIf StatusPicture = 2 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\3-2.wav"
        ElseIf StatusPicture = 3 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\3-3.wav"
        ElseIf StatusPicture = 4 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\3-4.wav"
        ElseIf StatusPicture = 5 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\3-5.wav"
        ElseIf StatusPicture = 6 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\3-6.wav"
        ElseIf StatusPicture = 7 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\3-7.wav"
        ElseIf StatusPicture = 8 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\3-8.wav"
        ElseIf StatusPicture = 9 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\3-9.wav"
        ElseIf StatusPicture = 10 Then
        W$ = "C:\Virtual Quran Lecturer\Wav\3-10.wav"
    End If
End If
PlaySound W$, 0, SND_FILENAME
End Sub
Private Sub CmdFundamental_Click()
LbStatus.Caption = "Basic Level"
StatusLevel = 1
StatusPicture = 1
CmdNext.Enabled = True
CmdPrevious.Enabled = False
PctArab.Enabled = True
CmdRecord.Enabled = True
CmdExample.Enabled = True
PrbAnalyze.Enabled = True
CmdFundamental.BackColor = &H80000010
CmdIntermediate.BackColor = &H8000000F
CmdAdvance.BackColor = &H8000000F
MsgBox "Basic: You will learn how to read the Hijaiyah letter correctly"
PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-1.jpg")
LbArab.Visible = True
LbArab.Caption = "Read : Alif"
LbArab2.Visible = False
LbArab3.Visible = False
End Sub
Private Sub CmdIntermediate_Click()
LbStatus.Caption = "Intermediate Level"
LbArab.Visible = True
StatusLevel = 2
StatusPicture = 1
CmdNext.Enabled = True
CmdPrevious.Enabled = False
PctArab.Enabled = True
CmdExample.Enabled = True
PrbAnalyze.Enabled = True
CmdRecord.Enabled = True
CmdIntermediate.BackColor = &H80000010
CmdFundamental.BackColor = &H8000000F
CmdAdvance.BackColor = &H8000000F
MsgBox "Intermediate: You will learn how to read the Qur'an Word depend on Tajwid rules"
PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\2-1.jpg")
LbArab2.Visible = True
LbArab3.Visible = False
LbArab.Caption = "Read : min hu"
LbArab2.Caption = "Tajwid : Idhar"
End Sub
Private Sub CmdAdvance_Click()
LbStatus.Caption = "Advance Level"
LbArab.Visible = True
StatusLevel = 3
StatusPicture = 1
CmdNext.Enabled = True
CmdPrevious.Enabled = False
CmdExample.Enabled = True
CmdRecord.Enabled = True
PctArab.Enabled = True
PrbAnalyze.Enabled = True
CmdAdvance.BackColor = &H80000010
CmdIntermediate.BackColor = &H8000000F
CmdFundamental.BackColor = &H8000000F
MsgBox "Advance: Qur'an word is more complicated, please read carefully"
PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\3-1.jpg")
LbArab2.Visible = True
LbArab3.Visible = True
LbArab.Caption = "Read : man aa ma na"
LbArab2.Caption = "Tajwid 1 : Idhar"
LbArab3.Caption = "Tajwid 2 : Mad Ashli"
End Sub
Private Sub CmdNext_Click()
CmdAnalyze.Enabled = False
CmdPrevious.Enabled = True
StatusPicture = StatusPicture + 1
If StatusLevel = 1 Then
    LbArab.Visible = True
    If StatusPicture = 2 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-2.jpg")
        LbArab.Caption = "Read : Ba"
    ElseIf StatusPicture = 3 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-3.jpg")
        LbArab.Caption = "Read : Ta"
    ElseIf StatusPicture = 4 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-4.jpg")
        LbArab.Caption = "Read : Tsa"
    ElseIf StatusPicture = 5 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-5.jpg")
        LbArab.Caption = "Read : Ja"
    ElseIf StatusPicture = 6 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-6.jpg")
        LbArab.Caption = "Read : Ha"
    ElseIf StatusPicture = 7 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-7.jpg")
        LbArab.Caption = "Read : Kha"
    ElseIf StatusPicture = 8 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-8.jpg")
        LbArab.Caption = "Read : Da"
    ElseIf StatusPicture = 9 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-9.jpg")
        LbArab.Caption = "Read : Dza"
    ElseIf StatusPicture = 10 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-10.jpg")
        LbArab.Caption = "Read : Ra"
        ElseIf StatusPicture = 11 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-11.jpg")
        LbArab.Caption = "Read : Za"
        ElseIf StatusPicture = 12 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-12.jpg")
        LbArab.Caption = "Read : Sa"
        ElseIf StatusPicture = 13 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-13.jpg")
        LbArab.Caption = "Read : Sya"
        ElseIf StatusPicture = 14 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-14.jpg")
        LbArab.Caption = "Read : Sho"
        ElseIf StatusPicture = 15 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-15.jpg")
        LbArab.Caption = "Read : Dho"
        ElseIf StatusPicture = 16 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-16.jpg")
        LbArab.Caption = "Read : Tho"
        ElseIf StatusPicture = 17 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-17.jpg")
        LbArab.Caption = "Read : Dlo"
        ElseIf StatusPicture = 18 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-18.jpg")
        LbArab.Caption = "Read : 'A"
        ElseIf StatusPicture = 19 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-19.jpg")
        LbArab.Caption = "Read : Gho"
        ElseIf StatusPicture = 20 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-20.jpg")
        LbArab.Caption = "Read : Fa"
        ElseIf StatusPicture = 21 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-21.jpg")
        LbArab.Caption = "Read : Qo"
        ElseIf StatusPicture = 22 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-22.jpg")
        LbArab.Caption = "Read : Ka"
        ElseIf StatusPicture = 23 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-23.jpg")
        LbArab.Caption = "Read : La"
        ElseIf StatusPicture = 24 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-24.jpg")
        LbArab.Caption = "Read : Ma"
        ElseIf StatusPicture = 25 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-25.jpg")
        LbArab.Caption = "Read : Na"
        ElseIf StatusPicture = 26 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-26.jpg")
        LbArab.Caption = "Read : Wa"
        ElseIf StatusPicture = 27 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-27.jpg")
        LbArab.Caption = "Read : Ha"
        ElseIf StatusPicture = 28 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-28.jpg")
        LbArab.Caption = "Read : A"
    ElseIf StatusPicture = 29 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-29.jpg")
        LbArab.Caption = "Read : Ya"
        CmdNext.Enabled = False
    End If
ElseIf StatusLevel = 2 Then
    LbArab.Visible = True
    If StatusPicture = 2 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\2-2.jpg")
        LbArab.Caption = "Read : man mana a"
        LbArab2.Caption = "Tajwid: Idghom Bighunnah"
    ElseIf StatusPicture = 3 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\2-3.jpg")
        LbArab.Caption = "Read : mal lam"
        LbArab2.Caption = "Tajwid: Idghom Bilaghunnah"
    ElseIf StatusPicture = 4 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\2-4.jpg")
        LbArab.Caption = "Read : mir rob bi him"
        LbArab2.Caption = "Tajwid: Idghom Bilaghunnah"
    ElseIf StatusPicture = 5 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\2-5.jpg")
        LbArab.Caption = "Read : yan ti qu"
        LbArab2.Caption = "Tajwid: Ikhfa'"
    ElseIf StatusPicture = 6 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\2-6.jpg")
        LbArab.Caption = "Read : min kum"
        LbArab2.Caption = "Tajwid: Ikhfa'"
        CmdNext.Enabled = False
    End If
ElseIf StatusLevel = 3 Then
        LbArab.Visible = True
    If StatusPicture = 2 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\3-2.jpg")
        LbArab.Caption = "Read : min nuur"
        LbArab2.Caption = "Tajwid 1 : Idghom Bighunnah"
        LbArab3.Caption = "Tajwid 2 : Mad Ashli"
    ElseIf StatusPicture = 3 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\3-3.jpg")
        LbArab.Caption = "Read : tam biih"
        LbArab2.Caption = "Tajwid 1 : Iqlab"
        LbArab3.Caption = "Tajwid 2 : Mad Ashli"
    ElseIf StatusPicture = 4 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\3-4.jpg")
        LbArab.Caption = "Read : an daa da"
        LbArab2.Caption = "Tajwid 1 : Ikhfa'"
        LbArab3.Caption = "Tajwid 2 : Mad Ashli"
        CmdNext.Enabled = False
    End If
End If
End Sub
Private Sub CmdPrevious_Click()
CmdAnalyze.Enabled = False
CmdNext.Enabled = True
StatusPicture = StatusPicture - 1
If StatusLevel = 1 Then
    LbArab.Visible = True
    If StatusPicture = 1 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-1.jpg")
        LbArab.Caption = "Read : Alif"
        CmdPrevious.Enabled = False
    ElseIf StatusPicture = 2 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-2.jpg")
        LbArab.Caption = "Read : Ba"
    ElseIf StatusPicture = 3 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-3.jpg")
        LbArab.Caption = "Read : Ta"
    ElseIf StatusPicture = 4 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-4.jpg")
        LbArab.Caption = "Read : Tsa"
    ElseIf StatusPicture = 5 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-5.jpg")
        LbArab.Caption = "Read : Ja"
    ElseIf StatusPicture = 6 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-6.jpg")
        LbArab.Caption = "Read : Ha"
    ElseIf StatusPicture = 7 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-7.jpg")
        LbArab.Caption = "Read : Kha"
    ElseIf StatusPicture = 8 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-8.jpg")
        LbArab.Caption = "Read : Da"
    ElseIf StatusPicture = 9 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-9.jpg")
        LbArab.Caption = "Read : Dza"
    ElseIf StatusPicture = 10 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-10.jpg")
        LbArab.Caption = "Read : Ro"
    ElseIf StatusPicture = 11 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-11.jpg")
        LbArab.Caption = "Read : Za"
    ElseIf StatusPicture = 12 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-12.jpg")
        LbArab.Caption = "Read : Sa"
    ElseIf StatusPicture = 13 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-13.jpg")
        LbArab.Caption = "Read : Sya"
    ElseIf StatusPicture = 14 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-14.jpg")
        LbArab.Caption = "Read : Sho"
    ElseIf StatusPicture = 15 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-15.jpg")
        LbArab.Caption = "Read : Dho"
    ElseIf StatusPicture = 16 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-16.jpg")
        LbArab.Caption = "Read : Tho"
    ElseIf StatusPicture = 17 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-17.jpg")
        LbArab.Caption = "Read : Dlo"
    ElseIf StatusPicture = 18 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-18.jpg")
        LbArab.Caption = "Read : 'A"
    ElseIf StatusPicture = 19 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-19.jpg")
        LbArab.Caption = "Read : Gho"
    ElseIf StatusPicture = 20 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-20.jpg")
        LbArab.Caption = "Read : Fa"
    ElseIf StatusPicture = 21 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-21.jpg")
        LbArab.Caption = "Read : Qo"
    ElseIf StatusPicture = 22 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-22.jpg")
        LbArab.Caption = "Read : Ka"
    ElseIf StatusPicture = 23 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-23.jpg")
        LbArab.Caption = "Read : La"
    ElseIf StatusPicture = 24 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-24.jpg")
        LbArab.Caption = "Read : Ma"
    ElseIf StatusPicture = 25 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-25.jpg")
        LbArab.Caption = "Read : Na"
    ElseIf StatusPicture = 26 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-26.jpg")
        LbArab.Caption = "Read : Wa"
    ElseIf StatusPicture = 27 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-27.jpg")
        LbArab.Caption = "Read : Ha"
    ElseIf StatusPicture = 28 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-28.jpg")
        LbArab.Caption = "Read : A"
    ElseIf StatusPicture = 29 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\1-29.jpg")
        LbArab.Caption = "Read : Ya"
        CmdNext.Enabled = False
    End If
ElseIf StatusLevel = 2 Then
    LbArab.Visible = True
    If StatusPicture = 1 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\2-1.jpg")
        LbArab.Caption = "Read: min hu"
        LbArab2.Caption = "Tajwid: Idhar"
        CmdPrevious.Enabled = False
    ElseIf StatusPicture = 2 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\2-2.jpg")
        LbArab.Caption = "Read: man ma na a"
        LbArab2.Caption = "Tajwid : Idhom Bighunnah"
    ElseIf StatusPicture = 3 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\2-3.jpg")
        LbArab.Caption = "Read: mal lam"
        LbArab2.Caption = "Tajwid : Idhom Bilaghunnah"
    ElseIf StatusPicture = 4 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\2-4.jpg")
        LbArab.Caption = "Read: mir rob bi him"
        LbArab2.Caption = "Tajwid : Idhom Bilaghunnah"
    ElseIf StatusPicture = 5 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\2-5.jpg")
        LbArab.Caption = "Read: yan thi qu"
        LbArab2.Caption = "Tajwid : Ikhfa'"
    ElseIf StatusPicture = 6 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\2-6.jpg")
        LbArab.Caption = "Read: min kum"
        LbArab2.Caption = "Tajwid : Ikhfa'"
        CmdPrevious.Enabled = False
    End If
ElseIf StatusLevel = 3 Then
    If StatusPicture = 1 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\3-1.jpg")
        LbArab.Caption = "Read: man aa ma na"
        LbArab2.Caption = "Tajwid 1 : Idhar"
        LbArab3.Caption = "Tajwid 2 : Mad Ashli"
        CmdPrevious.Enabled = False
    ElseIf StatusPicture = 2 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\3-2.jpg")
        LbArab.Caption = "Read: min nuur"
        LbArab2.Caption = "Tajwid 1 : Idghom Bighunnah"
        LbArab3.Caption = "Tajwid 2 : Mad Ashli"
    ElseIf StatusPicture = 3 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\3-3.jpg")
        LbArab.Caption = "Read: tam biih"
        LbArab2.Caption = "Tajwid 1 : Iqlab"
        LbArab3.Caption = "Tajwid 2 : Mad Ashli"
    ElseIf StatusPicture = 4 Then
        PctArab.Picture = LoadPicture("C:\Virtual Quran Lecturer\Images\3-4.jpg")
        LbArab.Caption = "Read: an daa da"
        LbArab2.Caption = "Tajwid 1 : Ikhfa'"
        LbArab3.Caption = "Tajwid 2 : Mad Ashli"
        CmdNext.Enabled = False
    End If
End If
End Sub
Private Sub CmdRecord_Click()
StatusRecord = StatusRecord + 1
If StatusRecord = 1 Then
    LbStatus.Caption = "Recording..."
    CmdNext.Enabled = False
    CmdPrevious.Enabled = False
    CmdExample.Enabled = False
    CmdFundamental.Enabled = False
    CmdIntermediate.Enabled = False
    CmdAdvance.Enabled = False
    CmdAnalyze.Enabled = False
    i = mciSendString("open new type waveaudio alias voice1", 0&, 0, 0)
    i = mciSendString("set voice1 bitspersample 8", 0&, 0, 0)
    i = mciSendString("set voice1 samplespersec 16000", 0&, 0, 0)
    i = mciSendString("set voice1 channels 1", 0&, 0, 0)
    i = mciSendString("record voice1", 0&, 0, 0)
    Timer1.Enabled = True
    CmdRecord.Caption = "Stop"
Else
    LbStatus.Caption = "Stop recording..."
    CmdAnalyze.Enabled = True
    CmdExample.Enabled = True
    CmdNext.Enabled = True
    CmdPrevious.Enabled = True
    CmdFundamental.Enabled = True
    CmdIntermediate.Enabled = True
    CmdAdvance.Enabled = True
    i = mciSendString("stop voice1", 0&, 0, 0)
    WaveFileName = "C:\Temp.wav"
    FileFound (WaveFileName)
        If ValidFile = True Then
            Kill WaveFileName
            i = mciSendString("save  voice1 C:\Temp.wav", 0&, 0, 0)
        Else
            i = mciSendString("save  voice1 C:\Temp.wav", 0&, 0, 0)
        End If
    i = mciSendString("close voice1", 0&, 0, 0)
    StatusRecord = 0
    CmdRecord.Caption = "Record"
End If
End Sub
Sub FrameBlocking(Signal, NumberSampelFrame, NumberFrame, NumberFrameSelected, selectedSignal, energySignalSelected)
ReDim Frame(NumberFrame, NumberSampelFrame) As Double
ReDim FrameWindowed(NumberFrame, NumberSampelFrame) As Double
ReDim EnergySignal(NumberFrame) As Double
ReDim Energy(NumberFrame) As Double
ReDim selectedSignal(NumberFrame, NumberSampelFrame) As Double
ReDim energySignalSelected(NumberFrame)
Dim NoFrame As Long
Dim NoSampling As Long

NumberFrameSelected = 0
For NoFrame = 1 To NumberFrame
    EnergySignal(NoFrame) = 0
    For NoSampling = 1 To NumberSampelFrame
        k = k + 1
        If k > 1 Then
            Signal(k) = Signal(k) - 0.95 * Signal(k - 1)
        End If
        Frame(NoFrame, NoSampling) = Signal(k)
        FrameWindowed(NoFrame, NoSampling) = Frame(NoFrame, NoSampling) * (0.54 - (0.46 * Cos(2 * 3.14159265 * NoSampling / 320)))
        EnergySignal(NoFrame) = EnergySignal(NoFrame) + FrameWindowed(NoFrame, NoSampling) ^ 2
    Next
    If NoFrame > 1 Then
        k = k - 160
    End If
    Energy(NoFrame) = Sqr(EnergySignal(NoFrame))
    If Energy(NoFrame) >= 0.2 Then
        NumberFrameSelected = NumberFrameSelected + 1
        For NoSampling = 1 To 320
            selectedSignal(NumberFrameSelected, NoSampling) = FrameWindowed(NoFrame, NoSampling)
        Next
        energySignalSelected(NumberFrameSelected) = Log(EnergySignal(NoFrame)) / Log(10)
    End If
Next
Erase Frame
Erase FrameWindowed
Erase EnergySignal
Erase Energy
End Sub
Sub DFT(selectedSignal, NumberSampelFrame, NumberDFT, NumberFrameSelected, Mag)
'Discrette Fourier Transform DFT N=1024'
ReDim XXR(NumberFrameSelected, NumberDFT)
ReDim XXI(NumberFrameSelected, NumberDFT)
ReDim Mag(NumberFrameSelected, NumberDFT)
Dim NoSampling As Integer
Dim N As Integer
For m = 1 To NumberFrameSelected
    For k = 1 To NumberDFT
        XXR(m, k) = 0
        XXI(m, k) = 0
        For N = 1 To NumberSampelFrame
            XXR(m, k) = XXR(m, k) + selectedSignal(m, N) * Cos(2 * 3.14 * k * N / 1024)
            XXI(m, k) = XXI(m, k) - selectedSignal(m, N) * Sin(2 * 3.14 * k * N / 1024)
            PrbAnalyze.Value = PrbAnalyze.Value + 5
        Next
        Mag(m, k) = Abs(Sqr(Abs(XXR(m, k) ^ 2 + XXI(m, k) ^ 2)))
    Next
Next
Erase XXR
Erase XXI
Erase selectedSignal
End Sub
Sub MFCC(Mag, NumberFrameSelected, NumberDFT, NumberMFCC, coeffMFCC, dMFCC, ddMFCC)
'Feature Extraction MFCC-Delta MFCC-Delta Delta MFCC- just rate'
ReDim f(NumberDFT) As Double
ReDim Tho(NumberMFCC + 1) As Double
ReDim Fc(NumberMFCC + 1) As Double
ReDim FrameFiltering(NumberFrameSelected, NumberMFCC) As Double
ReDim FrameMFC(NumberFrameSelected, NumberMFCC) As Double
ReDim coeffMFCC(NumberFrameSelected, NumberMFCC)
ReDim dMFCC(NumberFrameSelected, NumberMFCC)
ReDim ddMFCC(NumberFrameSelected, NumberMFCC)

Dim NoFrame As Integer
Dim NoSampling As Integer
Dim l As Integer
Dim ThoMax As Double
Dim ThoMin As Double
Dim ThoDelta As Double
For k = 1 To 512
    f(k) = k * 8000 / NumberDFT
Next

ThoMax = 2595 * (Log((8000 / 700) + 1) / Log(10))
ThoMin = 2595 * (Log(1) / Log(10))
ThoDelta = (ThoMax - ThoMin) / (NumberMFCC + 1)

For m = 1 To NumberMFCC
    Tho(m) = m * ThoDelta
    Fc(m + 1) = 700 * ((10 ^ (Tho(m) / 2595)) - 1)
Next

Fc(1) = 0
For NoFrame = 1 To NumberFrameSelected
    For m = 2 To NumberMFCC
        FrameFiltering(NoFrame, m) = 0
        For k = 1 To 512
            If f(k) < Fc(m - 1) Then
                FrameFiltering(NoFrame, m) = FrameFiltering(NoFrame, m) + Mag(NoFrame, k) * 0
            ElseIf f(k) >= Fc(m - 1) And f(k) < Fc(m) Then
                FrameFiltering(NoFrame, m) = FrameFiltering(NoFrame, m) + (Mag(NoFrame, k)) * ((f(k) - Fc(m - 1)) / (Fc(m) - Fc(m - 1)))
            ElseIf f(k) >= Fc(m) And f(k) < Fc(m + 1) Then
                 FrameFiltering(NoFrame, m) = FrameFiltering(NoFrame, m) + (Mag(NoFrame, k)) * ((f(k) - Fc(m + 1)) / (Fc(m) - Fc(m + 1)))
            ElseIf f(k) >= Fc(m + 1) Then
                FrameFiltering(NoFrame, m) = FrameFiltering(NoFrame, m) + Mag(NoFrame, k) * 0
            End If
       Next
    Next
Next

Erase Mag
Erase f
Erase Tho
Erase Fc
For NoFrame = 1 To NumberFrameSelected
    For m = 2 To NumberMFCC
        If FrameFiltering(NoFrame, m) <> 0 Then
                FrameMFC(NoFrame, m) = Log(Abs(FrameFiltering(NoFrame, m))) / Log(10)
            Else
                FrameMFC(NoFrame, m) = 0
        End If
    Next
Next
Erase FrameFiltering
Dim TempMFCC As Double

For NoFrame = 1 To NumberFrameSelected
    For l = 1 To NumberMFCC
        TempMFCC = 0
        For m = 2 To NumberMFCC
            TempMFCC = TempMFCC + (FrameMFC(NoFrame, m) * Cos((l * (180 / 7)) * ((m - 1) - 0.5)))
        Next
        coeffMFCC(NoFrame, l) = TempMFCC
        Debug.Print "MFCC :"; coeffMFCC(NoFrame, l)
    Next
Next
Dim Temp1 As Double
Dim Temp2 As Double
Dim Temp3 As Double
Dim Temp4 As Double
Dim p As Integer
For l = 1 To NumberMFCC
    For NoFrame = 1 To NumberFrameSelected
        If NoFrame > 2 And NoFrame < NumberFrameSelected - 2 Then
            Temp1 = 0
            Temp2 = 0
            For p = -2 To 2
                Temp1 = Temp1 + (coeffMFCC(NoFrame + p, l) - (coeffMFCC(NoFrame, l) * p))
                Temp2 = Temp2 + p ^ 2
            Next
            dMFCC(NoFrame, l) = Temp1 / Temp2
        Else
            dMFCC(NoFrame, l) = coeffMFCC(NoFrame, l)
        End If
    Next
    For NoFrame = 1 To NumberFrameSelected
        If NoFrame > 2 And NoFrame < NumberFrameSelected - 2 Then
            Temp1 = 0
            Temp2 = 0
            Temp3 = 0
            Temp4 = 0
            For p = -2 To 2
                Temp1 = Temp1 + p ^ 2
                Temp2 = Temp2 + p ^ 4
                Temp3 = Temp3 + coeffMFCC(NoFrame + p, l)
                Temp4 = Temp4 + (coeffMFCC(NoFrame + p, l) * (p ^ 2))
            Next
            ddMFCC(NoFrame, l) = ((Temp1 * Temp3) - (5 * Temp4)) / ((Temp1 ^ 2) - (5 * Temp2))
        Else
            ddMFCC(NoFrame, l) = dMFCC(NoFrame, l)
        End If
    Next
Next

Erase FrameMFC

End Sub
Function GetWaveInfo(ByVal WaveFile As Integer, ByRef SampleRate As Long, ByRef NumChannels As Integer, ByRef BitsPerSample As Integer, ByRef NumSamples As Long) As Boolean

    Dim wavhdr As WAVEHEADER
    Dim fmt As FMTCHUNK
    Dim chnk As CHUNKHEADER
    
    Dim msg As String
    Dim filepos As Long
    
    GetWaveInfo = True
    
    If LOF(WaveFile) < 45 Then
        GetWaveInfo = False
        Exit Function
    End If
    
    Get WaveFile, , chnk
    If chnk.Chunkname = "RIFF" Then
        Get WaveFile, , wavhdr
        If wavhdr.Rifftype <> "WAVE" Then
            GetWaveInfo = False
            Exit Function
        End If
    End If
    
    Do
        filepos = Loc(WaveFile)
        Get WaveFile, , chnk
        If chnk.Chunkname = "fmt " Then
            Get WaveFile, , fmt
            If fmt.Format <> 1 Then
                GetWaveInfo = False ' uncompressed PCM only
                Exit Function
            End If
        ElseIf chnk.Chunkname = "data" Then
            Exit Do
        End If
         
        filepos = filepos + chnk.Chunklen + 8 + 1
        If filepos > LOF(WaveFile) Then
            GetWaveInfo = False
            Exit Function
        End If
        
        Seek WaveFile, filepos
    
    Loop While EOF(WaveFile) = False
    
    If filepos > LOF(WaveFile) Or EOF(WaveFile) = True Then
        GetWaveInfo = False
        Exit Function
    End If
    
    NumSamples = chnk.Chunklen / (fmt.BitsPerSample / 8) / fmt.NumChannels
    NumChannels = fmt.NumChannels
    SampleRate = fmt.SampleRate
    BitsPerSample = fmt.BitsPerSample
    
    msg = msg + "Sample rate =" & Str$(SampleRate) & vbCrLf
    msg = msg + "Bits/Sample =" & Str$(BitsPerSample) & vbCrLf
    msg = msg + "Number of channels =" & Str$(NumChannels) & vbCrLf
    msg = msg + "Number of  samples =" & Str$(NumSamples) & vbCrLf
    'MsgBox msg
    
End Function



Private Sub Form_Load()
Call EnumDevices
Dim i As Long, RS As String, cb As Long, W$
RS = Space$(128)
W$ = "C:\Virtual Quran Lecturer\Wav\00.wav"
'PlaySound W$, 0, SND_FILENAME
MsgBox "First, you can read the tutorial about Makhraj and Tajwid in tutorial menu"
CmdNext.Enabled = False
CmdPrevious.Enabled = False
CmdExample.Enabled = False
StatusRecord = 0
End Sub

Private Sub MAbout_Click()
frmAbout.Show
End Sub

Private Sub MExit_Click()
Unload Me
End Sub

Private Sub MManual_Click()
i = ShellExecute(0, vbNullString, "C:\Virtual Quran Lecturer\Pdf\Manual.pdf", vbNullString, vbNullString, 1)
End Sub

Private Sub MTutorial_Click()
i = ShellExecute(0, vbNullString, "C:\Virtual Quran Lecturer\Pdf\Tutorial.pdf", vbNullString, vbNullString, 1)
End Sub
Sub GMM(NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
ReDim matrikBaris(NumberFrameSelected, 44)
ReDim matrikBarisAkhir(4, NumberFrameSelected)
Dim probability As Double
Dim bawah As Double
Dim exponent As Double
Dim Temp1 As Double
Dim Temp2 As Double
loglikelihood = 0
    For k = 1 To 4
        For i = 1 To NumberFrameSelected
            For m = 2 To 44
                If m = 2 Then
                matrikBaris(i, m) = (energySignalSelected(i) - parameter(k, m)) / parameter(k, m + 43)
                
                ElseIf m > 2 And m <= 16 Then
                matrikBaris(i, m) = (coeffMFCC(i, m - 2) - parameter(k, m)) / parameter(k, m + 43)
                
                ElseIf m > 16 And m <= 30 Then
                matrikBaris(i, m) = (dMFCC(i, m - 16) - parameter(k, m)) / parameter(k, m + 43)
                
                ElseIf m > 30 Then
                matrikBaris(i, m) = (ddMFCC(i, m - 30) - parameter(k, m)) / parameter(k, m + 43)
                
                End If
            Next
        Next
    
        For i = 1 To NumberFrameSelected
            matrikBarisAkhir(k, i) = 0
            For m = 2 To 44
            If m = 2 Then
                 matrikBarisAkhir(k, i) = matrikBarisAkhir(k, i) + (matrikBaris(i, m) * (energySignalSelected(i) - parameter(k, m)))
                'Debug.Print "MatrikBarisAkhir is about "; matrikBarisAkhir(k, i)
            ElseIf m > 2 And m <= 16 Then
                matrikBarisAkhir(k, i) = matrikBarisAkhir(k, i) + (matrikBaris(i, m) * (coeffMFCC(i, m - 2) - parameter(k, m)))
                'Debug.Print "MatrikBarisAkhir is about "; matrikBarisAkhir(k, i)
            ElseIf m > 16 And m <= 30 Then
                matrikBarisAkhir(k, i) = matrikBarisAkhir(k, i) + (matrikBaris(i, m) * (dMFCC(i, m - 16) - parameter(k, m)))
                'Debug.Print "MatrikBarisAkhir is about "; matrikBarisAkhir(k, i)
            ElseIf m > 30 Then
                matrikBarisAkhir(k, i) = matrikBarisAkhir(k, i) + (matrikBaris(i, m) * (ddMFCC(i, m - 30) - parameter(k, m)))
                'Debug.Print "MatrikBarisAkhir is about "; matrikBarisAkhir(k, i)
            End If
            Next
        Next
    Next
    For i = 1 To NumberFrameSelected
    probability = 0
        For k = 1 To 4
            bawah = ((2 * 3.14) ^ 22) * (Sqr(determinan(k)))
            Debug.Print "Bawah is about "; bawah
            exponent = -matrikBarisAkhir(k, i) / 2 / 50
            Debug.Print "exponent is about "; exponent
            probability = probability + (parameter(k, 1) / bawah) * (2.716 ^ exponent)
            Debug.Print "Probability is about "; probability
        Next
        Debug.Print "Probability is about "; probability
        If probability <> 0 Then
        loglikelihood = loglikelihood + (Log(probability) / Log(10))
        Debug.Print "loglikelihood is about "; loglikelihood
        End If
    Next
    loglikelihood = loglikelihood / NumberFrameSelected
    'MsgBox "Your loglikelihood is about " & loglikelihood
End Sub
Sub GMMComplicated(start, NumberFrameSelected, parameter, energySignalSelected, coeffMFCC, dMFCC, ddMFCC, determinan, loglikelihood)
ReDim matrikBaris(NumberFrameSelected, 44)
ReDim matrikBarisAkhir(4, NumberFrameSelected)
Dim probability As Double
Dim bawah As Double
Dim exponent As Double
Dim Temp1 As Double
Dim Temp2 As Double
loglikelihood = 0
    For k = 1 To 4
        For i = start To NumberFrameSelected
            For m = 2 To 44
                If m = 2 Then
                matrikBaris(i, m) = (energySignalSelected(i) - parameter(k, m)) / parameter(k, m + 43)
                
                ElseIf m > 2 And m <= 16 Then
                matrikBaris(i, m) = (coeffMFCC(i, m - 2) - parameter(k, m)) / parameter(k, m + 43)
                
                ElseIf m > 16 And m <= 30 Then
                matrikBaris(i, m) = (dMFCC(i, m - 16) - parameter(k, m)) / parameter(k, m + 43)
                
                ElseIf m > 30 Then
                matrikBaris(i, m) = (ddMFCC(i, m - 30) - parameter(k, m)) / parameter(k, m + 43)
                
                End If
            Next
        Next
    
        For i = start To NumberFrameSelected
            matrikBarisAkhir(k, i) = 0
            For m = 2 To 44
            If m = 2 Then
                 matrikBarisAkhir(k, i) = matrikBarisAkhir(k, i) + (matrikBaris(i, m) * (energySignalSelected(i) - parameter(k, m)))
                'Debug.Print "MatrikBarisAkhir is about "; matrikBarisAkhir(k, i)
            ElseIf m > 2 And m <= 16 Then
                matrikBarisAkhir(k, i) = matrikBarisAkhir(k, i) + (matrikBaris(i, m) * (coeffMFCC(i, m - 2) - parameter(k, m)))
                'Debug.Print "MatrikBarisAkhir is about "; matrikBarisAkhir(k, i)
            ElseIf m > 16 And m <= 30 Then
                matrikBarisAkhir(k, i) = matrikBarisAkhir(k, i) + (matrikBaris(i, m) * (dMFCC(i, m - 16) - parameter(k, m)))
                'Debug.Print "MatrikBarisAkhir is about "; matrikBarisAkhir(k, i)
            ElseIf m > 30 Then
                matrikBarisAkhir(k, i) = matrikBarisAkhir(k, i) + (matrikBaris(i, m) * (ddMFCC(i, m - 30) - parameter(k, m)))
                'Debug.Print "MatrikBarisAkhir is about "; matrikBarisAkhir(k, i)
            End If
            Next
        Next
    Next
    For i = start To NumberFrameSelected
    probability = 0
        For k = 1 To 4
            bawah = ((2 * 3.14) ^ 22) * (Sqr(determinan(k)))
            Debug.Print "Bawah is about "; bawah
            exponent = -matrikBarisAkhir(k, i) / 2 / 100
            Debug.Print "exponent is about "; exponent
            probability = probability + (parameter(k, 1) / bawah) * (2.716 ^ exponent)
            Debug.Print "Probability is about "; probability
        Next
        Debug.Print "Probability is about "; probability
        If probability <> 0 Then
        loglikelihood = loglikelihood + (Log(probability) / Log(10))
        Debug.Print "loglikelihood is about "; loglikelihood
        End If
    Next
    loglikelihood = loglikelihood / NumberFrameSelected
    'MsgBox "Your loglikelihood is about " & loglikelihood
End Sub
Sub GetDataTxt(parameter, determinan)
ReDim parameter(4, 87)
ReDim determinan(4)
determinan(1) = 1
determinan(2) = 1
determinan(3) = 1
determinan(4) = 1

For i = 1 To 87
    For k = 1 To 4
    Input #1, parameter(k, i)
    If i > 44 Then
        determinan(k) = determinan(k) * parameter(k, i)
    End If
        'Debug.Print "Matrik parameter("; k; ","; i; "): "; parameter(k, i)
    Next
Next
'Debug.Print "Determinan 1"; determinan(1)
'Debug.Print "Determinan 2"; determinan(2)
'Debug.Print "Determinan 3"; determinan(3)
'Debug.Print "Determinan 4"; determinan(4)
    
End Sub

