VERSION 5.00
Begin VB.Form frmSplash 
   BorderStyle     =   0  'None
   ClientHeight    =   3240
   ClientLeft      =   5895
   ClientTop       =   3495
   ClientWidth     =   8340
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   ForeColor       =   &H00004080&
   Icon            =   "frmSplash.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   NegotiateMenus  =   0   'False
   ScaleHeight     =   3240
   ScaleWidth      =   8340
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox Picture1 
      BackColor       =   &H00404080&
      FillColor       =   &H00004080&
      Height          =   4095
      Left            =   -240
      Picture         =   "frmSplash.frx":000C
      ScaleHeight     =   4035
      ScaleMode       =   0  'User
      ScaleWidth      =   10395
      TabIndex        =   0
      Top             =   -120
      Width           =   10455
      Begin VB.CommandButton CmdStart 
         Appearance      =   0  'Flat
         BackColor       =   &H00808080&
         Caption         =   "Start"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   6840
         MaskColor       =   &H00000080&
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   2880
         Width           =   1335
      End
   End
End
Attribute VB_Name = "frmSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub CmdStart_Click()
Unload Me
FMainVQL.Show
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    Unload Me
End Sub

Private Sub Form_Load()
    'lblVersion.Caption = "Version " & App.Major & "." & App.Minor & "." & App.Revision
    'lblProductName.Caption = App.Title
End Sub

